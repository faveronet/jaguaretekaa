from django.urls import path
from . import views

app_name = "sitio"
urlpatterns = [
    path('', views.index, name="index"),
    path('acerca_de', views.acerca_de, name="acerca_de"),
    path('carrito_vista', views.carrito_vista, name="carrito_vista"),    
    path('carrito_agregar/<int:producto_id>', views.carrito_agregar, name="carrito_agregar"),      
    path('carrito_eliminar/<int:producto_id>', views.carrito_eliminar, name="carrito_eliminar"),      
    path('carrito_vaciar', views.carrito_vaciar, name="carrito_vaciar"),
    path('registrarse', views.registrarse, name="registrarse"),
    path('filtro_busqueda', views.filtro_busqueda, name="filtro_busqueda"),
    path('filtro_categorias/<int:categoria_id>', views.filtro_categorias, name="filtro_categorias"),    
    path('producto_vista/<int:producto_id>', views.producto_vista, name="producto_vista"),
    path('producto_alta', views.producto_alta, name="producto_alta"),
    path('producto_edicion/<int:producto_id>', views.producto_edicion, name="producto_edicion"),
    path('producto_eliminacion/<int:producto_id>', views.producto_eliminacion, name="producto_eliminacion"),

]

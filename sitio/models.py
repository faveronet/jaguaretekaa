from django.core.exceptions import ValidationError
from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Categoria(models.Model):
    descripcion = models.CharField(max_length=64, null=False)
    def __str__(self):
        return f"{self.descripcion}"

class Producto(models.Model):
    titulo = models.CharField(max_length=250, null=False)
    imagen = models.FileField(upload_to='imagenes/')
    descripcion = models.CharField(max_length=2000, null=False)
    precio = models.FloatField()
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE, related_name="producto_categoria")

class Carrito(models.Model): 
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, related_name="carrito_usuario")
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE, related_name="carrito_producto") 
    
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Producto

class FormProducto(forms.ModelForm):
    class Meta:
        model = Producto
        fields = ('titulo', 'categoria', 'descripcion', 'precio', 'imagen')
        widgets = {
            'titulo': forms.TextInput(attrs={'class': 'pub_titulo'}),
            'descripcion': forms.Textarea(attrs={'class': 'pub_descripcion'}),
            'imagen': forms.FileInput(attrs={'name':'imagen_adjunta', 'class': 'pub_imagen'}),
            'precio': forms.TextInput(attrs={'class': 'pub_precio'}),
        }


class RegistroForm(UserCreationForm):
    email = forms.EmailField(
        label='Email', widget=forms.TextInput(attrs={'class': 'email'}), max_length=254, required=True, help_text='')
    username = forms.CharField(
        label='Usuario', widget=forms.TextInput(attrs={'class': 'usuario'}), max_length=150, required=True, help_text='')
    password1 = forms.CharField(
        label='Password', widget=forms.PasswordInput(), max_length=30, required=True, help_text='')
    password2 = forms.CharField(
        label='Repetir Password', widget=forms.PasswordInput(), max_length=30, required=True, help_text='')
    class Meta:
        model = User
        fields = ('email', 'username', 'password1', 'password2', )
from django.contrib.auth.models import User
from django.db.models.query import QuerySet
from django.db.models import Q
from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect, render, get_object_or_404
from django.urls import reverse
from .forms import *
from .models import Categoria, Producto, Carrito
from django.utils import timezone
from django.contrib.auth.decorators import login_required
# from django.contrib.auth.decorators import permission_required

# Create your views here.

def index(request):
    return render(request,"blog/index.html", {
        "lista_productos": Producto.objects.all().order_by('-id')[:10],
        "lista_categorias": Categoria.objects.all(),
    })

def registrarse(request):
    if request.method == 'POST':
        form = RegistroForm(request.POST)
        if form.is_valid():
            form.save()          
            return HttpResponseRedirect(reverse('login'))
    else:
        form = RegistroForm()
    return render(request, 'registration/registro.html', {
        'form': form
        })    

def filtro_busqueda(request):
    queryset = request.GET['buscador']
    if queryset:
        productos = Producto.objects.filter(
            Q(titulo__icontains = queryset) | 
            Q(descripcion__icontains = queryset) 
        ).distinct()
    return render(request,"blog/busqueda.html", {
        "lista_productos": productos,
        "lista_categorias": Categoria.objects.all(),
    })

def filtro_categorias(request, categoria_id):
    una_categoria = get_object_or_404(Categoria, id=categoria_id)
    queryset = Producto.objects.all()
    queryset = queryset.filter(categoria=una_categoria)
    return render(request,"blog/busqueda.html", {
        "lista_productos": queryset,
        "lista_categorias": Categoria.objects.all(),
        "categoria_seleccionada": una_categoria,
    })    

def producto_vista(request, producto_id):
    un_producto = get_object_or_404(Producto, id=producto_id)
    return render(request, "blog/producto_vista.html", {
        "producto": un_producto,
        "lista_categorias": Categoria.objects.all(),
    })

def producto_alta(request):
    if request.method == "POST":
        user = User.objects.get(username=request.user)
        form = FormProducto(request.POST, request.FILES, instance=Producto(imagen=request.FILES['imagen']))      
        if form.is_valid():
            form.save()
            return redirect("sitio:index")          
    else:
        form = FormProducto(initial={})
        return render(request, "blog/producto_nuevo.html", {
            "form": form,
            "lista_categorias": Categoria.objects.all(),
        })


def producto_edicion(request, producto_id):
    un_producto = get_object_or_404(Producto, id=producto_id)
    if request.method == "POST":  
        user = User.objects.get(username=request.user)   
        form = FormProducto(data=request.POST, files=request.FILES, instance=un_producto)
        if form.is_valid():
            form.save()
            return redirect("sitio:index")
    else:
        form = FormProducto(instance = un_producto)
        return render(request, 'blog/producto_edicion.html', {
            "producto": un_producto,
            "form": form,
            "lista_categorias": Categoria.objects.all(),
        })

def producto_eliminacion(request, producto_id):
    un_producto = get_object_or_404(Producto, id=producto_id)
    un_producto.delete()
    return redirect("sitio:index")

def acerca_de(request):
    return render(request, "blog/acerca_de.html", {
        "lista_categorias": Categoria.objects.all(),
    })

def carrito_vista(request):
    return render(request, "blog/carrito_vista.html", {
        "lista_carrito": Carrito.objects.filter(usuario=request.user.id).select_related("producto","usuario"),
        "lista_categorias": Categoria.objects.all(),
    })
 
def carrito_agregar(request, producto_id):
    carrito = Carrito.objects.create(producto_id=producto_id, usuario_id=request.user.id)
    carrito.save
    return redirect("sitio:index")

def carrito_eliminar(request, producto_id):
    carrito = get_object_or_404(Carrito, producto_id=producto_id, usuario_id=request.user.id)
    carrito.delete()
    return render(request, "blog/carrito_vista.html", {
        "lista_carrito": Carrito.objects.filter(usuario=request.user.id).select_related("producto","usuario"),
        "lista_categorias": Categoria.objects.all(),
    })

def carrito_vaciar(request):
    Carrito.objects.filter(usuario=request.user.id).all().delete()
    return render(request, "blog/carrito_vista.html", {
        "lista_carrito": Carrito.objects.filter(usuario=request.user.id).select_related("producto","usuario"),
        "lista_categorias": Categoria.objects.all(),
    })